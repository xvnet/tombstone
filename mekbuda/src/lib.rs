#![feature(new_uninit)]
#![allow(incomplete_features)]
#![feature(async_fn_in_trait)]
#![feature(async_closure)]
#![feature(pointer_byte_offsets)]

pub mod chain;
pub mod config;
pub mod dns;
pub mod inet;
pub mod resolver;
pub mod subnet;
pub mod tun;
