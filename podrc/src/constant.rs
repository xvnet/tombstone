pub const LABEL_NO_PURGE: &str = "org.eu.xvnet.podrc.no_purge";
pub const LABEL_NO_PURGE_VAL: &str = "true";

pub const PODMAN_SYSTEM_NETWORK: &str = "podman";

pub const ENV_PURGE_VOLS: &str = "PODRC_PURGE_VOLS";
pub const ENV_PURGE_RUNNING_CTRS: &str = "PODRC_PURGE_RUNNING_CTRS";
