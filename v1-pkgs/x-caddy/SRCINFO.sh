# shellcheck shell=bash disable=SC2034

# src type=alpinelinux
alpinever='3.19.1'

# src type=git repo=https://github.com/caddyserver/xcaddy.git branch=master
xcaddyver='51aadd62b72c2fc8df5e97eb6a42c0904e4c8c8b'
