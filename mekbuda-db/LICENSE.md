# License

For all content not explicitly stated (excluded LICENSE\* files), it is released under the CC-BY-SA-4.0 license.

If the file has an explicit license statement, the following licenses can be used:

- CC0 1.0 Universal
- The Unlicense
- Creative Commons Attribution 4.0 International
- Creative Commons Attribution-ShareAlike 4.0 International

Copies of these licenses have been included in the repository.
